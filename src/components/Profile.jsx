import React, { Component } from 'react';
import {
  isSignInPending,
  loadUserData,
  Person,
  getFile,
  putFile,
  lookupProfile
} from 'blockstack';

const avatarFallbackImage = 'https://s3.amazonaws.com/onename/avatar-placeholder.png';

export default class Profile extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      person: {
        name() {
          return 'Anonymous';
        },
        avatarUrl() {
          return avatarFallbackImage;
        },
      },
      username: "",
      newStatus: "",
      statuses: [],
      statusIndex: 0,
      isLoading: false
    };
  }

  isLocal() {
    return this.props.match.params.username ? false : true
  }
    
  render() {
    const { handleSignOut } = this.props;
    const { person } = this.state;
    const { username } = this.state;

    return (
      !isSignInPending() && person ?
      <div className="container">
        <div className="row">
          <div className="col-md-offset-3 col-md-6">
            <div className="col-md-12">
              <div className="avatar-section">
                <img
                  src={ person.avatarUrl() ? person.avatarUrl() : avatarFallbackImage }
                  className="img-rounded avatar"
                  id="avatar-image"
                />
                <div className="username">
                  <h1>
                    <span id="heading-name">{ person.name() ? person.name()
                      : 'Nameless Person' }</span>
                    </h1>
                  <span>{username}</span>
                  {
                    this.isLocal() && (
                      <span>
                        &nbsp;|&nbsp;
                        <a onClick={handleSignOut.bind(this)}>(Logout)</a>
                      </span>
                    )
                  }
                </div>
              </div>
            </div>

            {
              this.isLocal() && (
                <div className="new-status">
                  <div className="col-md-12">
                    <textarea className="input-status"
                      value={this.state.newStatus}
                      onChange={e => this.handleNewStatusChange(e)}
                      placeholder="What's on your mind?"
                    />
                  </div>
                  <div className="col-md-12">
                    <button
                      className="btn btn-primary btn-lg"
                      onClick={e => this.handleNewStatusSubmit(e)}
                    >
                      Submit
                    </button>
                  </div>
                  <div className="col-md-12">
                    <button
                      className="btn btn-primary btn-lg"
                      onClick={e => this.handleDeleteStatusSubmit(e)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              )
            }
            <div className="new-status">
              <div className="col-md-12 statuses">
                {this.state.isLoading && <span>Loading...</span>}
                {this.state.statuses.map((status) => (
                  <div className="status" key={status.id}>
                    {status.text}
                  </div>
                ))}
              </div>              
            </div>

          </div>
        </div>
      </div> : null
    );
  }

  componentDidMount() {
    this.fetchData()
  }
  
  fetchData() {
    this.setState({ isLoading: true })
    if (this.isLocal()) {
      debugger;
      const options = { decrypt: false }
      getFile('statuses.json', options)
        .then((file) => {
          var statuses = JSON.parse(file || '[]')
          this.setState({
            person: new Person(loadUserData().profile),
            username: loadUserData().username,
            statusIndex: statuses.length,
            statuses: statuses,
          })
          console.log(this.state.person.name());
        })
        .finally(() => {
          this.setState({ isLoading: false })
        })
    } else {
      const username = this.props.match.params.username
  
      lookupProfile(username)
        .then((profile) => {
          this.setState({
            person: new Person(profile),
            username: username
          })
          debugger;
          const options = { username: username, decrypt: false, zoneFileLookupURL: "https://core.blockstack.org/v1/names" }
          getFile('statuses.json', options)
            .then((file) => {
              var statuses = JSON.parse(file || '[]')
              this.setState({
                statusIndex: statuses.length,
                statuses: statuses
              })
            })
            .catch((error) => {
              debugger;
              console.log('could not fetch statuses')
            })
            .finally(() => {
              this.setState({ isLoading: false })
            })        
    
        })
        .catch((error) => {
          console.log('could not resolve profile')
        })
        .finally(() => {
          this.setState({ isLoading: false })
        })        

    }
  }

  saveNewStatus(statusText) {
    let statuses = this.state.statuses
  
    let status = {
      id: this.state.statusIndex++,
      text: statusText.trim(),
      created_at: Date.now()
    }
  
    debugger;
    statuses.unshift(status)
    const options = { encrypt: false }
    putFile('statuses.json', JSON.stringify(statuses), options)
      .then(() => {
        this.setState({
          statuses: statuses
        })
      })
  }  

  deleteStatus(statusText) {
    let statuses = this.state.statuses,
        newStatuses = []
    ;
  
    this.state.statusIndex = 0;
    for (let i = 0; i < statuses.length; i++) {
      if (statuses[i].text === statusText)
        continue;

        let status = {
          id: this.state.statusIndex++,
          text: statuses[i].text,
          created_at: statuses[i].created_at
        }
      
        newStatuses.push(status);
    }
    const options = { encrypt: false }
    putFile('statuses.json', JSON.stringify(newStatuses), options)
      .then(() => {
        this.setState({
          statuses: newStatuses
        })
      })
  }  


  handleNewStatusChange(event) {
    this.setState({newStatus: event.target.value})
  }
  
  handleNewStatusSubmit(event) {
    this.saveNewStatus(this.state.newStatus)
    this.setState({
      newStatus: ""
    })
  }

  handleDeleteStatusSubmit(event) {
    debugger;
    this.deleteStatus(this.state.newStatus)
    this.setState({
      newStatus: ""
    })
  }

  componentWillMount() {
    this.setState({
      person: new Person(loadUserData().profile),
      username: loadUserData().username
    });
  }
}
